package com.jmi.restdemo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;



@RunWith(SpringRunner.class)
@WebMvcTest(GreetingController.class)
public class RestdemoApplicationTests {

   @Autowired
   private MockMvc mockMvc;

   @MockBean
   private GreetingRepository greetingRepository;

   private static String asJsonString(final Object obj) {
      try {
         return new ObjectMapper().writeValueAsString(obj);
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
   }

   @Test
   public void contextLoads() {
   }

   @Before
   public void setUpMockMvc() {
   }

   @Test
   public void createsGreeting() throws Exception {
      Greeting greeting = new Greeting("prueba");

      System.out.println(asJsonString(greeting));

      mockMvc.perform(post(GreetingController.URL_POST_GREETING).contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(greeting)))
            .andExpect(status().isOk());
      
      Greeting retGreeting = new Greeting(10, "prueba", 100);
      when(greetingRepository.findById("1")).thenReturn(retGreeting);

      mockMvc.perform(get(GreetingController.URL_GET_GREETING, "1").contentType(MediaType.APPLICATION_JSON))
         .andExpect(status().isOk())
         .andExpect(jsonPath("$.content").value(retGreeting.getContent()))
         .andExpect(jsonPath("$.id").value(retGreeting.getId()))
         .andExpect(jsonPath("$.time", not(-1)));
   }
}
