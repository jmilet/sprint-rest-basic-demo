package com.jmi.restdemo;

import java.io.Serializable;

public class Greeting implements Serializable {

  private static final long serialVersionUID = 1L;

  private final long id;
  private final String content;
  private final long time;

  public Greeting(long id, String content, long time) {
    this.id = id;
    this.content = content;
    this.time = time;
  }

  public Greeting(String content) {
    this.id = -1;
    this.content = content;
    this.time = -1;
  }

  public long getId() {
    return id;
  }

  public String getContent() {
    return content;
  }

  public long getTime() {
    return time;
  }
}