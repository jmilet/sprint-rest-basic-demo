package com.jmi.restdemo;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private final AtomicLong counter = new AtomicLong();
    private GreetingRepository greetingRepository;

    private static final String URL_ROOT = "/greeting";

    public static final String URL_POST_GREETING = URL_ROOT;
    public static final String URL_GET_ALL_GREETINGS = URL_ROOT + "s";
    public static final String URL_GET_GREETING = URL_ROOT + "/{id}";

    public GreetingController(GreetingRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

    private static long now() {
        return Instant.now().toEpochMilli();
    }

    @RequestMapping(value = URL_GET_ALL_GREETINGS, method = RequestMethod.GET)
    public Map<String, Greeting> getAllgreetings() {
        return this.greetingRepository.findAll();
    }

    @RequestMapping(value = URL_GET_GREETING, method = RequestMethod.GET)
    public Greeting getGreeting(@PathVariable("id") String id) {
        return this.greetingRepository.findById(id);
    }

    @RequestMapping(value = URL_POST_GREETING, method = RequestMethod.POST)
    public Greeting createGreeting(@RequestBody Greeting greeting) {
       
        System.out.println("-----------------------");
       
        final long id = counter.incrementAndGet();
        final long timestamp = now();

        Greeting newGreeting = new Greeting(id, greeting.getContent(), timestamp);
        this.greetingRepository.save(newGreeting);

        return newGreeting;
    }
}