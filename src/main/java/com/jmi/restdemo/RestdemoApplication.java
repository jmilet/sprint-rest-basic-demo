package com.jmi.restdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootApplication
public class RestdemoApplication {

	@Bean
	static JedisConnectionFactory jedisConnectionFactory() {
		return new JedisConnectionFactory();
	}

	@Bean
	static RedisTemplate<String, Greeting> redisTemplate() {
		RedisTemplate<String, Greeting> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(jedisConnectionFactory());
		
		return redisTemplate;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(RestdemoApplication.class, args);
	}

}

