package com.jmi.restdemo;

import java.util.Map;

public interface GreetingRepository {
   void save(Greeting greeting);
   Map<String, Greeting> findAll();
   Greeting findById(String id);
   void upate(Greeting greeting);
   void delete(String id);
}