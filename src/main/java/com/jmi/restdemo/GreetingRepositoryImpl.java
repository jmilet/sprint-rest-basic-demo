package com.jmi.restdemo;

import java.util.Map;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public class GreetingRepositoryImpl implements GreetingRepository {

   private static final String HASH_NAME = "GREETING";

   private HashOperations<String, String, Greeting> hashOperations;

   public GreetingRepositoryImpl(RedisTemplate<String, Greeting> redisTemplate) {
      this.hashOperations = redisTemplate.opsForHash();
   }

   @Override
   public void save(Greeting greeting) {
      this.hashOperations.put(HASH_NAME, String.valueOf(greeting.getId()), greeting);
   }

   @Override
   public Map<String, Greeting> findAll() {
      return this.hashOperations.entries(HASH_NAME);
   }

   @Override
   public Greeting findById(String id) {
      return this.hashOperations.get(HASH_NAME, id);
   }

   @Override
   public void upate(Greeting greeting) {
      this.save(greeting);
   }

   @Override
   public void delete(String id) {
      this.hashOperations.delete(HASH_NAME, String.valueOf(id));
   }
}