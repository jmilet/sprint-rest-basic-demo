Start the app
```
./mvnw spring-boot:run
```

Get all greetings
```
http localhost:8080/greetings
```

Get one greeting
```
http -v GET localhost:8080/greeting/100
```

Post one greeting
```
http -v POST localhost:8080/greeting content="hola que tal"
```